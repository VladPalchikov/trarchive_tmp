<?php

namespace App\Http\Controllers;

use App\Models\Attribute;
use App\Models\AttributeValue;
use App\Models\Ed;
use App\Models\File;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class EdController extends Controller
{
    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return array|JsonResponse
     */
    public function index(Request $request)
    {
        $dataValidator = Validator::make($request->all(), Ed::$filterParamsRules);

        if ($dataValidator->fails()) {
            return response()->json(['code' => 400, 'message' => $dataValidator->errors()], 400);
        }

//        DB::enableQueryLog();
        $eds = Ed::with('source', 'dossier', 'attributeValue', 'attributeValue.attribute', 'file', 'file.role', 'file.extension', 'file.rel')
            ->withFilters($request)->withOrder($request)->withPaginate($request);

//        dd(DB::getQueryLog());

        return $eds;
    }

    /**
     * Create a new resource.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request)
    {
        $dataValidator = Validator::make($request->all(), [
            'data' => 'required|json',
            'files' => 'required|array',
            'files.*' => 'file|max:100000' //~100mb
        ]);

        if ($dataValidator->fails()) {
            return response()->json(['code' => 400, 'message' => $dataValidator->errors()], 400);
        }

        $data = json_decode($request->input('data'), true);

        $validator = Validator::make($data, [
            'name' => 'required|string',
            'num' => 'required|string',
            'reg_date' => 'required|date|date_format:Y-m-d',
            'ed_type_id' => 'integer|min:1',
            'source_id' => 'required|integer|min:1|exists:sources,id',
            'source_ed_id' => 'required|string',
            'save_period' => 'integer|min:1',
            'dos_id' => 'integer|min:1|exists:dossiers,id',
            'attr' => 'array',
            'attr.*.attr_id' => 'required|integer|min:1|exists:attributes,id',
            'files' => 'required|array',
            'files.*.fr_id' => 'required|integer|min:1|exists:file_roles,id',
            //'files.*.parent_file' => 'string',
        ]);

        if ($validator->fails()) {
            return response()->json(['code' => 400, 'message' => $validator->errors()], 400);
        }

        // TODO catch rollback
        $document = new Ed($data);

        DB::transaction(function () use ($data, $request, $document) {
            $document->save();
            $files = $request->file('files');
            if (is_array($files) || is_object($files)) {
                foreach ($files as $file) {
                    $isCorrectFile = false;
                    $index = -1;
                    foreach ($data['files'] as $fileInfo) {
                        $index++;
                        if ($fileInfo['file'] === $file->getClientOriginalName()) {
                            $isCorrectFile = true;
                            break;
                        }
                    }
                    if (!$isCorrectFile) {
                        //FIXME Кидать исключение
                        return response()->json(['code' => 400, 'message' => 'Название файла в метаданных не совпадает с его реальным названием'], 400);
                    }

                    if ($file->isValid()) {
                        $path = "ead/$document->id/";
                        Storage::disk('public_media')->putFileAs($path, $file, $file->getClientOriginalName());
                        $publicPath = basename(Storage::disk('public_media')->getAdapter()->getPathPrefix()) . '/' . $path . $file->getClientOriginalName();
                        $fileModel = new File([
                            "name" => $file->getClientOriginalName(),
                            "size" => round($file->getClientSize() / 1024 / 1024, 2),
                            "role_id" => $data['files'][$index]['fr_id'], // Взять из data
                            "ed_id" => $document->id,
                            "path" => $publicPath
                        ]);
                        $fileModel->setExtensionId($file->extension());

                        $fileModel->save();
                        if ($data['files'][$index]['parent_file']) {
                            $parentFile = File::where([
                                'name' => $data['files'][$index]['parent_file'],
                                'ed_id' => $document->id,
                            ])->get();
                            DB::table($fileModel->getRelTableName())->insert([
                                'f1_id' => $fileModel->id,
                                'f2_id' => $parentFile[0]->id,
                            ]);
                        }
                    }
                }
            }

            if(!empty($data['attr'])) {
                foreach ($data['attr'] as $item) {
                    $attr = Attribute::findOrFail($item['attr_id']);
                    $attrValue = new AttributeValue();
                    $result = $attrValue->checkType($item);

                    if ($result == $attr->type) {
                        $attrValue = new AttributeValue([
                            "value" => $item['attr_value'],
                            "attribute_type_id" => $item['attr_id'],
                            "ed_id" => $document->id,
                        ]);

                        $attrValue->save();
                    } else {
                        return response()->json(['code' => 500, 'message' => "Ошибка типов данных"], 500);
                    }
                }
            }
        });

        return response()->json(['code' => 201, 'message' => url("/ead/eds/$document->id")], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Collection|Model|JsonResponse
     */
    public function show($id)
    {
        try {
            $document = Ed::with('source', 'dossier', 'attributeValue', 'attributeValue.attribute', 'file', 'file.role', 'file.extension', 'file.rel')
                ->findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'code' => 404,
                'message' => 'Document Not Found.',
            ], 404);
        }

        return $document;
    }
}
