<?php

namespace App\Http\Controllers;

use App\Models\FileExtension;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FileExtensionController extends Controller
{
    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return array|JsonResponse
     */
    public function index(Request $request)
    {
        $dataValidator = Validator::make($request->all(), FileExtension::$validationRules);

        if ($dataValidator->fails()) {
            return response()->json(['code' => 400, 'message' => $dataValidator->errors()], 400);
        }
        return [
            'file_extensions' => FileExtension::withFilter($request)->get()
        ];
    }
}
