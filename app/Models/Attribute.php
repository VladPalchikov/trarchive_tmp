<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Attribute extends Model
{
    protected $hidden = ['created_at', 'updated_at'];
    public static $validationRules = [
        'parent_attr_id' => 'integer|min:1',
        'q' => 'string'
    ];

    public function scopeWithFilters($query, Request $request)
    {

        return $query->when($request->query('parent_attr_id'), function (Builder $query, $parentId) {
            $query->where('parent_id', $parentId);
        })
            ->when($request->query('q'), function (Builder $query, $q) {
                $query->where(function ($query) use ($q) {
                    $query->where(DB::raw('LOWER(name)'), 'LIKE', "%" . mb_strtolower($q) . "%")
                        ->orWhere(DB::raw('LOWER(description)'), 'LIKE', "%" . mb_strtolower($q) . "%");
                });
            });
    }

    public function toArray()
    {
        return [
            "id"=> $this->id,
            "code"=> $this->code,
            "parent_id"=> $this->parent_id,
            "name"=> $this->name,
            "descr"=> $this->description,
            "value_type"=> $this->type,
            "etalon_attr_id"=> $this->etalon_attr_id,
            "is_etalon"=> $this->is_etalon
        ];
    }
}
