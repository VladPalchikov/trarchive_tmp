<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AttributeValue extends Model
{
    protected $hidden = ['created_at', 'updated_at', 'ed_id', 'attribute_type_id'];
    protected $fillable = ['value', 'attribute_type_id', 'ed_id'];

    protected $attrType = ['integer', 'string', 'date', 'boolean'];

    public function attribute()
    {
        return $this->belongsTo(Attribute::class, 'attribute_type_id', 'id');
    }

    public function checkType($data)
    {
        $attrValId = 0;
        foreach ($data as $val) {
            foreach ($this->attrType as $key => $type) {
                if ($type == gettype($val['attr_value'])) {
                    $attrValId = $key + 1;
                }

                if(empty($val['attr_value'])) {
                    $attrValId = 4;
                }
            }
            if ($attrValId == 2) {
                $date = date_parse($val['attr_value']);

                if(!empty($date['year'])) {
                    $attrValId = 3;
                }
            }
        }
        if ($attrValId > 0 ) {
            return $attrValId;
        } else {
            return false;
        }
    }

    public function getValueAttribute ($value)
    {
        $type = $this->attrType[$this->attribute_type_id-1];

        if ($type == 'date') {
            $type = 'string';
        }

        switch ($type) {
            case 'integer':
                return intval($value);
            case 'string':
                return strval($value);
            case 'boolean':
                return $value == 'true' ? true : false;
        }
    }

    public function toArray()
    {
        return [
            'id' => $this->id,
            'value' => $this->value,
            'attr' => $this->attribute,
        ];
    }
}
