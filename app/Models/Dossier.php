<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Dossier extends BaseModel
{
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->hidden = array_merge(parent::getHidden(), ['source_id', 'index']);
    }
}
