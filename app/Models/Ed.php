<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Ed extends Model
{
    protected $hidden = ['created_at', 'updated_at', 'ed_type_id', 'source_id', 'dossier_id'];
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public static $filterParamsRules = [
        'name' => 'string',
        'num' => 'string',
        'source' => 'integer|min:1|exists:sources,id',
        'start_reg_date' => 'date|date_format:Y-m-d',
        'end_reg_date' => 'date|date_format:Y-m-d',
        'sort' => 'string',
        'page' => 'array',
        'page[number]' => 'integer|min:1',
        'page.size' => 'integer|min:1'
    ];

    public function source()
    {
        return $this->belongsTo(Source::class);
    }

    public function dossier()
    {
        return $this->belongsTo(Dossier::class);
    }

    public function attributeValue()
    {
        return $this->hasMany(AttributeValue::class, 'ed_id', 'id');
    }
    public function file()
    {
        return $this->hasMany(File::class);
    }

    public function scopeWithFilters($query, Request $request)
    {
        return $query->when($request->query('name'), function (Builder $query, $name) {
            $query->where(DB::raw("LOWER(" . $this->getTable() . ".name)"), 'LIKE', "%" . mb_strtolower($name) . "%");
        })
            ->when($request->query('num'), function (Builder $query, $num) {
                $query->where(DB::raw('LOWER(num)'), 'LIKE', "%" . mb_strtolower($num) . "%");
            })
            ->when($request->query('source'), function (Builder $query, $source) {
                $query->where($this->getTable() . '.source_id', $source);
            })
            ->when($request->query('start_reg_date'), function (Builder $query, $start) {
                $query->whereDate('reg_date', '>=', $start);
            })
            ->when($request->query('end_reg_date'), function (Builder $query, $end) {
                $query->whereDate('reg_date', '<=', $end);
            });
    }

    public function scopeWithOrder($query, Request $request) {
        $sorts = explode(',', $request->query('sort', $this->getTable() . '.id'));
        $columnNames = $this->getColumnNames();
        foreach ($sorts as $sortColumn) {
            $sortDirection = starts_with($sortColumn, '-') ? 'desc' : 'asc';
            $sortColumn = ltrim($sortColumn, '-');
            if (in_array($sortColumn, $columnNames)) {
                $query->orderBy($this->getTable() . '.' . $sortColumn, $sortDirection);
            } else {
                if ($sortColumn === 'source') {
                    //TODO Подумать как убрать вложенный if
                    if (!self::isJoined($query, Source::getTableName())) {
                        $query->join(Source::getTableName(), 'sources.id', '=', 'eds.source_id');
                    }
                    $query->orderBy(Source::getTableName() . '.name', $sortDirection);
                }
            }
        }

        return $query;
    }

    public function scopeWithPaginate($query, Request $request) {
        $pageParam = $request->query('page');
        $perPage = 50;
        $number = 1;
        if (!empty($pageParam)) {
            $number = isset($pageParam['number']) ? $pageParam['number'] : $number;
            $perPage = isset($pageParam['size']) ? $pageParam['size'] : $perPage;
        }
        $filterParams = $request->query->all();

        return $query->paginate($perPage, ['*'], 'page[number]', $number)->appends($filterParams);
    }

    public function toArray()
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "num" => $this->num,
            "reg_date" => $this->reg_date,
            "source_ed_id" => $this->source_ed_id,
            "save_period" => $this->save_period,
            "source" => $this->source,
            "dossier" => $this->dossier,
            "attr_values" => $this->attributeValue,
            "files" => $this->file,
        ];
    }

    public function getColumnNames() {
        return DB::getSchemaBuilder()->getColumnListing($this->getTable());
    }

    //FIXME Вынести в обертку над model
    public static function isJoined($query, $table)
    {
        $joins = $query->getQuery()->joins;
        if($joins == null) {
            return false;
        }

        foreach ($joins as $join) {
            if ($join->table == $table) {
                return true;
            }
        }

        return false;
    }
}
