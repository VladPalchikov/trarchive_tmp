<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EdInRegister extends Model
{
    protected $table = 'ed_in_register';
    public function register()
    {
        return $this->belongsTo(Register::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function ed()
    {
        return $this->belongsTo(Ed::class);
    }

    public function registerPart()
    {
        return $this->belongsTo(RegisterPart::class);
    }
}
