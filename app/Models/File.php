<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class File extends Model
{
    protected $hidden = ['created_at', 'updated_at', 'fe_id', 'role_id', 'ed_id'];
    protected $guarded = ['id', 'created_at', 'updated_at'];
    protected $casts = [
        'size' => 'float',
    ];

    public function role()
    {
        return $this->belongsTo(FileRole::class);
    }

    public function extension()
    {
        return $this->belongsTo(FileExtension::class, 'fe_id');
    }

    public function rel()
    {
        return $this->belongsToMany(File::class, 'files_files', 'f1_id', 'f2_id');
    }

    public function getRelTableName() {
        return 'files_files';
    }

    /**
     * Устанавливает id расширения, если оно есть в базе, иначе ставит id элемента "Не установлено"
     * @param $extName string
     */
    public function setExtensionId($extName)
    {
        $emptyExtName = 'Не установлен';

        $extensions = DB::table(FileExtension::getTableName())->select('id', 'name')
            ->whereIn(DB::raw('LOWER(name)'), [mb_strtolower($extName), mb_strtolower($emptyExtName)])->get();

        if ($extensions->count() > 1) {
            $ext = $extensions->first(function ($ext) use ($extName) {
                return mb_strtolower($ext->name) === mb_strtolower($extName);
            });
            $this->fe_id = $ext->id;
        } else {
            $this->fe_id = $extensions->get(0)->id;
        }
    }

    public function toArray()
    {
        return [
            "id" => $this->id,
            "file_role" => $this->role,
            "uri" => $this->path,
            "size" => $this->size,
            "name" => $this->name,
            "file_ext" => $this->extension,
            "parent_file" => $this->rel
        ];
    }
}
