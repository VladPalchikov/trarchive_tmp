<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Found extends Model
{
    protected $table = 'found';

    public function archive()
    {
        return $this->belongsTo(Archive::class);
    }
}
