<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Register extends Model
{
    protected $table = 'register';

    public function registerType()
    {
        return $this->belongsTo(RegisterType::class);
    }

    public function archive()
    {
        return $this->belongsTo(Archive::class);
    }

    public function found()
    {
        return $this->belongsTo(Found::class);
    }
}
