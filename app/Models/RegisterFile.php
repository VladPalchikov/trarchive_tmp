<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RegisterFile extends Model
{
    protected $table = 'register_file';

    public function register()
    {
        return $this->belongsTo(Register::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
