<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RegisterPart extends Model
{
    protected $table = 'register_part';

    public function parent()
    {
        return $this->belongsTo(RegisterPart::class, 'parent_id', 'id');
    }

    public function register()
    {
        return $this->belongsTo(Register::class);
    }
}
