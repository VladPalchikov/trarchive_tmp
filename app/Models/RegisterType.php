<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RegisterType extends Model
{
    protected $table = 'register_type';
}
