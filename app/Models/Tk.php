<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tk extends Model
{
    protected $table = 'tk';

    public function statusMessages()
    {
        return $this->hasMany(TkStatusMessage::class);
    }

    public function archive()
    {
        return $this->belongsTo(Archive::class);
    }

    public function found()
    {
        return $this->belongsTo(Found::class);
    }

    public function ed()
    {
        return $this->belongsTo(Ed::class);
    }

    public function register()
    {
        return $this->belongsTo(Register::class);
    }

    public function tkStatus()
    {
        return $this->belongsTo(TkStatus::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
