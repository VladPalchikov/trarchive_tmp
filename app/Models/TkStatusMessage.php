<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TkStatusMessage extends Model
{
    protected $table = 'tk_status_message';

    public function messageType()
    {
        return $this->belongsTo(MessageType::class);
    }

    public function tk()
    {
        return $this->belongsTo(Tk::class);
    }
}
