<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = 'user';

    public function epFiles()
    {
        return $this->hasMany(EpFile::class);
    }
}
