<?php


namespace App\Utils;
use Illuminate\Pagination\LengthAwarePaginator;


class EdsPaginator extends LengthAwarePaginator
{
    public function toArray()
    {
        return [
            'count' => $this->total(),
            'next' => $this->nextPageUrl(),
            'prev' => $this->previousPageUrl(),
            'first' => $this->url(1),
            'last' => $this->url($this->lastPage()),
            'eds' => $this->items->toArray(),
        ];
    }
}