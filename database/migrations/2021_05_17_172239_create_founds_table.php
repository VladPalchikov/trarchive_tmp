<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFoundsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('found', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('num');
            $table->text('description')->nullable();
            $table->unsignedInteger('archive_id');
            $table->foreign('archive_id')->references('id')->on('archive');
            $table->timestamps();
            $table->index('archive_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('found');
    }
}
