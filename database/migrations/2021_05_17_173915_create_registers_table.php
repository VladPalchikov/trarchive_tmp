<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('register', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('num');
            $table->unsignedInteger('year')->nullable();
            $table->dateTime('create_date')->nullable();
            $table->unsignedInteger('register_type_id');
            $table->unsignedInteger('archive_id');
            $table->unsignedInteger('found_id');
            $table->foreign('register_type_id')->references('id')->on('register_type');
            $table->foreign('archive_id')->references('id')->on('archive');
            $table->foreign('found_id')->references('id')->on('found');
            $table->timestamps();
            $table->index('archive_id');
            $table->index('found_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('register');
    }
}
