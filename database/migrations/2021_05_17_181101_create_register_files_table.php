<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegisterFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('register_file', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type');
            $table->dateTime('sign_date');
            $table->string('name');
            $table->unsignedInteger('register_id');
            $table->unsignedInteger('user_id');
            $table->foreign('register_id')->references('id')->on('register');
            $table->foreign('user_id')->references('id')->on('user');
            $table->timestamps();
            $table->index('register_id');
            $table->index('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('register_file');
    }
}
