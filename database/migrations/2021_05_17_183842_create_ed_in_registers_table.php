<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEdInRegistersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ed_in_register', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('ed_add_date');
            $table->unsignedInteger('ed_id')->nullable();
            $table->unsignedInteger('register_id')->nullable();
            $table->unsignedInteger('register_part_id')->nullable();
            $table->unsignedInteger('user_id')->nullable();
            $table->foreign('ed_id')->references('id')->on('eds');
            $table->foreign('register_id')->references('id')->on('register');
            $table->foreign('register_part_id')->references('id')->on('register_part');
            $table->foreign('user_id')->references('id')->on('user');
            $table->timestamps();

            $table->index('ed_id');
            $table->index('register_id');
            $table->index('register_part_id');
            $table->index('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ed_in_register');
    }
}
