<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tk', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tk_type');
            $table->dateTime('send_date')->nullable();
            $table->dateTime('form_date')->nullable();
            $table->dateTime('sign_end_date')->nullable();
            $table->string('tk_path');
            $table->string('ep_path');
            $table->unsignedInteger('archive_id')->nullable();
            $table->unsignedInteger('found_id')->nullable();
            $table->unsignedInteger('ed_id')->nullable();
            $table->unsignedInteger('user_id')->nullable();
            $table->unsignedInteger('tk_status_id')->nullable();
            $table->unsignedInteger('register_id')->nullable();
            $table->foreign('archive_id')->references('id')->on('archive');
            $table->foreign('found_id')->references('id')->on('found');
            $table->foreign('ed_id')->references('id')->on('eds');
            $table->foreign('user_id')->references('id')->on('user');
            $table->foreign('tk_status_id')->references('id')->on('tk_status');
            $table->foreign('register_id')->references('id')->on('register');

            $table->index('archive_id');
            $table->index('found_id');
            $table->index('ed_id');
            $table->index('user_id');
            $table->index('tk_status_id');
            $table->index('register_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tk');
    }
}
