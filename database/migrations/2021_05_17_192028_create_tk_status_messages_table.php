<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTkStatusMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tk_status_message', function (Blueprint $table) {
            $table->increments('id');
            $table->text('message');
            $table->dateTime('date')->nullable();
            $table->unsignedInteger('mt_id')->nullable();
            $table->unsignedInteger('tk_id')->nullable();
            $table->foreign('mt_id')->references('id')->on('message_type');
            $table->foreign('tk_id')->references('id')->on('tk');
            $table->index('mt_id');
            $table->index('tk_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tk_status_message');
    }
}
