<?php

use JeroenZwart\CsvSeeder\CsvSeeder;

class ArchiveTableSeeder extends CsvSeeder
{
    public function __construct()
    {
        $this->file = '/database/seeds/csvs/ARCHIVE.csv';
        $this->tablename = 'archive';
        $this->truncate = false;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Recommended when importing larger CSVs
        DB::disableQueryLog();
        parent::run();
    }
}
