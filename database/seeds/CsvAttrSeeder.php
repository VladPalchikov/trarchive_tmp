<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use JeroenZwart\CsvSeeder\CsvSeeder;
use App\Csvdata;
use Illuminate\Support\Facades\Schema;

class CsvAttrSeeder extends CsvSeeder
{
    public function __construct()
    {
        $this->tablename = 'ed_attribute_types';
        $this->truncate = false;
        $this->file = '/database/seeds/csvs/ATTR.csv';
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();

        DB::disableQueryLog();
        parent::run();
    }
}
