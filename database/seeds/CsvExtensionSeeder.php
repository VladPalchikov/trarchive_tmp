<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use JeroenZwart\CsvSeeder\CsvSeeder;
use App\Csvdata;
use Illuminate\Support\Facades\Schema;

class CsvExtensionSeeder extends CsvSeeder
{
    public function __construct()
    {
        $this->tablename = 'file_extensions';
        $this->truncate = false;
        $this->file = '/database/seeds/csvs/FILE_EXTENSION.csv';
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();

        DB::disableQueryLog();
        parent::run();
    }
}
