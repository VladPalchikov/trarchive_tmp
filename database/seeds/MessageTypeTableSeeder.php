<?php

use JeroenZwart\CsvSeeder\CsvSeeder;

class MessageTypeTableSeeder extends CsvSeeder
{
    public function __construct()
    {
        $this->file = '/database/seeds/csvs/MESSAGE_TYPE.csv';
        $this->tablename = 'message_type';
        $this->truncate = false;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Recommended when importing larger CSVs
        DB::disableQueryLog();
        parent::run();
    }
}
