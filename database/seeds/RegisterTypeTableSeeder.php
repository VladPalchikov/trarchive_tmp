<?php

use JeroenZwart\CsvSeeder\CsvSeeder;

class RegisterTypeTableSeeder extends CsvSeeder
{
    public function __construct()
    {
        $this->file = '/database/seeds/csvs/REGISTER_TYPE.csv';
        $this->tablename = 'register_type';
        $this->truncate = false;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Recommended when importing larger CSVs
        DB::disableQueryLog();
        parent::run();
    }
}
