<?php

use JeroenZwart\CsvSeeder\CsvSeeder;

class TkStatusTableSeeder extends CsvSeeder
{
    public function __construct()
    {
        $this->file = '/database/seeds/csvs/TK_STATUS.csv';
        $this->tablename = 'tk_status';
        $this->truncate = false;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Recommended when importing larger CSVs
        DB::disableQueryLog();
        parent::run();
    }
}
