<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});


Route::prefix('api/ead')->group(function () {
    Route::prefix('eds')->group(function () {
        Route::post('/', 'EdController@create');
        Route::get('', 'EdController@index');
        Route::get('/{id}', 'EdController@show')->where('id', '[0-9]+');
        Route::post('/post', 'EdController@create');
    });

    Route::get('attrs', 'AttributeController@index');

    Route::prefix('nsi')->group(function () {
        Route::get('sources', 'SourceController@index');
        Route::get('file_roles', 'FileRoleController@index');
        Route::get('file_extensions', 'FileExtensionController@index');
    });
});

Route::fallback(function () {
    return response()->json([
        'code' => 404,
        'message' => 'Page Not Found.',
    ], 404);
});
